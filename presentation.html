<!DOCTYPE html>
<html>
  <head>
    <title>Introduction to D8 Migrations</title>
    <meta charset="utf-8">
    <style>
      @import url(https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz);
      @import url(https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic);
      @import url(https://fonts.googleapis.com/css?family=Ubuntu+Mono:400,700,400italic);

      body { font-family: 'Droid Serif'; }
      h1, h2, h3 {
        font-family: 'Yanone Kaffeesatz';
        font-weight: normal;
      }
      .remark-code, .remark-inline-code { font-family: 'Ubuntu Mono'; font-size: 80%}
    
      img {
        max-width: 100%;
      }

      .smaller {
        font-size: smaller;
      }

      .pathway img {max-width: 50%}

      .logos img {
        margin: 0 1em;
      }
    
    </style>
  </head>
  <body>
    <textarea id="source">

class: center

# Introduction to D8 Migrations

<br>
**Audience**  
developers new to creating D7/D8 migrations  

<br>

**Technical level**  
low / intermediate

<br>

There is still time to go to another session.  
  
(I won't be offended)

---
class: center, middle
## Introduction to D8 Migrations
#### Joeri Poesen - Pathway Education

jpoesen@pathwayeducation.eu  

twitter: @jpoesen  
d.o.: /u/jpoesen  
gitlab: jpoesen  

---
class: center
<br>
.pathway[
![](assets/images/pathway.svg)
]
<br>
.logos[
![](assets/images/ec.png)
![](assets/images/alcatel.jpg)
<br><br>
![](assets/images/un.jpg)
![](assets/images/usaid.jpg)
<br><br>
![](assets/images/ou.jpg)
![](assets/images/codeenigma.jpg)
]

---
### Agenda

1. What is the D8 migration framework

2. "Migrate Drupal" vs "Drupal Migrations"

3. Tools

4. Example 1: Importing CSV data

5. Example 2: Importing XML data

6. Example 3: Importing referential data

7. Example 4: Dependent migrations

8. Interesting plugins


---
### Source Code

Presentation:  
https://gitlab.com/jpoesen/introduction-to-d8-migrations

Sample code:  
https://gitlab.com/jpoesen/product_migration

---
### What is the D8 Migration framework

* developer-oriented framework: no user interface; only code

* powers the D6/D7 &rarr; D8 core upgrade mechanism

* migrate data from *any* source into Drupal

* D7: part of contrib

* D8: part of core

---
### "Migrate Drupal" vs "Drupal Migrations"

#### Migrate Drupal
* upgrade _core_ from D6/D7 to D8

#### Drupal Migrations
* import data from any supported source
* import data from D8 to D8

---
### Data Migration Principles

![ETL](assets/images/etl.png)

Based on the **Extract, Transform, Load (ETL)** concept:

* *extract* data from a data source

* *transform* the retrieved data (optional)

* *load* (transformed) data into the target system

---

### Tools

#### Core
* enable the Migrate module

#### Contrib
* install [drush](https://github.com/drush-ops/drush)

* install and enable [migrate_tools](https://www.drupal.org/project/migrate_tools) (contrib)

* install and enable [migrate_plus](https://www.drupal.org/project/migrate_plus) (contrib)

* install and enable [migrate_source_csv](https://www.drupal.org/project/migrate_source_csv) (contrib)

---

### Tools

#### drush

* command-line interface for Drupal

#### migrate_tools

* provides drush commands to start, stop, restart, and rollback migrations  

#### migrate_plus

* provides additional source, destination, process plugins

#### migrate_source_csv

* provides a CSV migration source type

---
class: center, middle
## Example 1
### Importing CSV data

---

### Example CSV source

.smaller[&rarr; sites/default/files/products.csv]
```csv
"product_id","product_name","product_SKU","product_description"
"101","Dell XPS 13","R082578","cool developer laptop."
"102","Apple iphone 7","R098882","Overpriced phone."
"108","Logitech mouse","R278634","Bluetooth mouse."
```

.smaller[&rarr; Same data displayed differently:]
```csv
| product_id | product_name   | product_SKU | product_description    |
| ---------- | -------------- | ----------- | ---------------------- |
|        101 | Dell XPS 13    | R082578     | Cool developer laptop. |
|        102 | Apple iphone X | R098882     | Overpriced phone.      |
|        108 | Logitech mouse | R278634     | Bluetooth mouse.       |

```
---
class: middle, center
#### Requirements:
##### For each record in the CSV file, create a Product node
##### (assuming node type Product exists)
---

### Migration module file structure

.smaller[&rarr; /modules/product_migration]
```yaml 
├── config
│   └── install
│       └── migrate_plus.migration.migrate_products_csv.yml
└── product_migration.info.yml

```

1. Add info file.

2. Add migration definition.

---

### The info file

.smaller[&rarr; /modules/product_migration/product_migration.info.yml]
```yaml
name: 'Example Product Migration'
description: 'Example migration from CSV.'
type: module
core: 8.x
package: 'Training'

dependencies:
- migrate_tools
- migrate_plus
- migrate_source_csv
  ```

---

### The Migration definition file

Often organised into 5 sections:

1. General info

2. **Source** description (*extract*)

3. **Process** description (*transform*)

4. **Destination** description (*load*)

5. Dependency info

---
### Migration Section 1: general info

.smaller[&rarr; migrate_plus.migration.migrate_products_csv.yml]

```yaml
# Pattern: migrate_plus.migration.THIS_IS_THE_ID.yml
id: migrate_products_csv

label: Product migration (CSV)

```
---
### Migration Section 2: source description

Each source type has its own configuration settings.

```yaml
source:
  plugin: csv

  # Path to the import file.
  path: public://products.csv

  # The number of rows at the beginning which are not data.
  header_row_count: 1

  # User-friendly name for each of the source columns.
  column_names:
    0:
      product_id: Product ID
    1:
      product_name: Product Name
    2:
      product_sku: Product SKU
    3:
      product_description: Product Description

  # Field(s) in the CSV file that make each record unique.
  keys:
    - product_id
```

---
### Migration Section 3: destination description

```yaml
destination:
  # What should be created for each source record.
  plugin: entity:node
```

---
### Migration Section 4: process description

```yaml
process:
  # Specify destination node for each record.
  # In this case *every* record should become a Product node.
  type:
    plugin: default_value
    default_value: product

  # Field mapping between the destination fields (product node) 
  # and source fields (CSV columns).
  
  title: product_name
  
  body: product_description
  
  field_product_sku: product_sku
  
  field_original_product_id: product_id
```

---
### Migration Section 5: dependency description

```yaml
dependencies:
  enforced:
    # Add this module's name to ensure this migration gets removed from 
    # active configuration when the module is uninstalled.
    module:
      - product_migration

```
---
### Next steps

Enable product_migration module

```txt
drush en product_migration -y
```

If your module was already enabled, **uninstall it and reinstall it**:

```txt
drush pm-uninstall product_migration -y; drush en product_migration -y
```

In fact &rarr; 

---

**Uninstall it and reinstall** your migration module *after every change* in a migration definition.
```txt
drush pm-uninstall product_migration -y; drush en product_migration -y
```

Why?
Migrations are *configuration data*, only read when the module is installed.

---
### Running the migration

```yaml
drush migrate:import migrate_products_csv
```

or

```yaml
drush migrate:import --update migrate_products_csv
```

<br>

### Rolling back the migration

```yaml 
drush migrate:rollback migrate_products_csv
``` 

---
class: center, middle
## Example 2
### Importing XML data

---
### Example XML source

.smaller[&rarr; sites/default/files/products.xml]
```xml
<?xml version="1.0" encoding="UTF-8"?>
<products>
  <product>
    <product_id>211</product_id>
    <product_name>Xerox CPX12</product_name>
    <product_sku>X345856</product_sku>
    <product_description>Copy Fax combo machine.</product_description>
  </product>
  <product>
    <product_id>217</product_id>
    <product_name>Xylophone</product_name>
    <product_sku>X001673</product_sku>
    <product_description>Plink Plink. Plonk plonk.</product_description>
  </product>

</products>
```

---
### Migration definition

* source: XML source plugin instead of CSV source plugin

* destination: same as the CSV example

* process: same as the CSV example


---
#### Migration source description (1/2):

```yaml
source:
  plugin: url
  data_fetcher_plugin: http
  data_parser_plugin: xml

  # Import file location. Can be local or remote.
  urls: public://products.xml

  # Xpath query that will yield the records to import.
  item_selector: /products/product

  fields:
    -
      name: product_id
      label: 'Product ID'
      selector: product_id
    -
      name: product_name
      label: 'Product Name'
      selector: product_name
    -
      name: product_sku
      label: 'Product SKU'
      selector: product_sku
    -
      name: product_description
      label: 'Product Description'
      selector: product_description

```

---
#### Migration source description (2/2)

(continued from previous slide)

```yaml
# Field(s) that make each record unique
  ids:
    product_id:
      type: string
```
---
class: center, middle
## Example 3
### Importing referential data
#### (import taxonomy terms from CSV)

---
### Example CSV source

.smaller[&rarr; sites/default/files/categories.csv]
```csv
"category_id","category_name","category_parent_id"
"1","entertainment",""
"2","arts supplies",""
"3","computer hardware","4"
"4","computers",""
"5","computer software","4"
```

.smaller[&rarr; Same data displayed differently:]
```csv
| category_id | category_name     | category_parent_id |
| ----------- | ----------------- | ------------------ |
|           1 | entertainment     |                    |
|           2 | arts supplies     |                    |
|           3 | computer hardware |                  4 |
|           4 | computers         |                    |
|           5 | computer software |                  4 |
```
---
### Migration Section 1: general info

```yaml
id: migrate_categories_csv

label: Category migration (CSV)

```
---
### Migration Section 2: source description

```yaml
plugin: csv

# Path to the import file.
path: public://categories.csv

# The number of rows at the beginning which are not data.
header_row_count: 1

# For each of the CSV columns we're interested in, specify a user friendly name.
column_names:
  0:
    category_id: Category ID
  1:
    category_name: Category Name
  2:
    category_parent_id: Category Parent ID

# Fields in the CSV file that makes each record unique.
keys:
  - category_id
```

---
### Migration Section 3: destination description

```yaml
destination:
  # What should be created for each source record.
* plugin: entity:taxonomy_term
```

---
### Migration Section 4: process description

```yaml
process:
  # Field mapping.

  # Vocabulary machine name.
* vid:
*   plugin: default_value
*   default_value: categories

  # Term name.
  name: category_name

* # Parent term.
* parent:
*   plugin: migration_lookup
*   migration: migrate_categories_csv
*   source: category_parent_id
```

---
### Running the migration

Execute the migration:
```yaml
drush migrate:import migrate_categories_csv
```

Output:

```txt
[notice] Processed 6 items (4 created, 2 updated, 0 failed, 0 ignored) 
- done with 'migrate_categories_csv'

```

---
class: center, middle
## Example 4
### Dependent Migrations
#### (import products + taxonomy terms from CSV)

---
### Example CSV source

.smaller[&rarr; sites/default/files/products_with_category.csv]
```csv
"product_id","product_name","product_SKU","product_description", "product_category"
"222","Logitech USB Cable","R94836","For connecting stuff.", "3"
"239","MS Office 2019","R90983","Full Office suite.", "5"
"299","HP Deskjet 9000","R233634","Fancy printer.", "3"
```

.smaller[&rarr; Same data displayed differently:]
```csv
| product_id | product_name       | product_SKU | product_description   | product_category |
| ---------- | ------------------ | ----------- | --------------------- | ---------------- |
|        222 | Logitech USB Cable | R94836      | For connecting stuff. |  3               |
|        239 | MS Office 2019     | R90983      | Full Office suite.    |  5               |
|        299 | HP Deskjet 9000    | R233634     | Fancy printer.        |  3               |

```

---
### Migration source description

```yaml
source:

  plugin: csv

  path: public://products_with_category.csv

  header_row_count: 1

  # Field descriptions.
  column_names:
    0:
      product_id: Product ID
    1:
      product_name: Product Name
    2:
      product_sku: Product SKU
    3:
      product_description: Product Description
*   4:
*     product_category: Product Category

  # What makes each source record unique.
  keys:
    - product_id
```

---
### Migration destination description

```yaml
destination:
  # What should be created for each source record.
  plugin: entity:node
```


---
### Migration process description

```yaml
process:

  type:
    plugin: default_value
    default_value: product

  # Field mapping.

  title: product_name
  
  body: product_description
  
  field_product_sku: product_sku
  
  field_original_product_id: product_id
  
* field_category:
*   plugin: migration_lookup
*   migration: migrate_categories_csv
*   source: product_category
```

---
### Migration dependency description

```yaml
dependencies:
  enforced:
    # Add this module's name to ensure this migration gets removed from active
    # configuration when the module is uninstalled.
    module:
      - product_migration

*migration_dependencies:
* required:
*   - migrate_categories_csv
```

---
class: center, middle

### Some interesting plugins


---
### Interesting source plugin providers

* https://drupal.org/project/migrate_source_csv

* https://drupal.org/project/migrate_plus
  * XML
  * JSON
  * SOAP

---
### Interesting core process plugins (1/3)

#### callback

```yaml
process:
  destination_field:
    plugin: callback
    callable: strtolower
    source: some_source_field
```

---
### Interesting core process plugins (2/3)

#### concat

```yaml
process:
  destination_field:
    plugin: concat
    source:
      - some_source_field
      - other_source_field
    delimiter: /
```

---
### Interesting core process plugins (3/3)

#### static_map

```yaml
process:
  destination_field:
    plugin: static_map
    source: some_source_field
    map: 
      Y: yes
      N: no
      M: maybe

```
---

### Further reading

* https://www.drupal.org/docs/8/api/migrate-api
* drush migrate commands: https://www.drupal.org/node/1561820

---

class: center, middle

### Thank you.


    </textarea>
    <script src="assets/js/remark-latest.min.js">
    </script>
    <script>
      var slideshow = remark.create({
        highlightLines: true
      });
    </script>
  </body>
</html>